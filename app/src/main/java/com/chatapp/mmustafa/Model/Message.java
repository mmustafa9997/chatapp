package com.chatapp.mmustafa.Model;

public class Message {

    public String dateTime;
    public String message;
    public String name;

    public Message(String dateTime, String message , String name) {

        this.dateTime = dateTime;
        this.message = message;
    }
    public Message(String name, String message) {

        this.name = name;
        this.message = message;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
