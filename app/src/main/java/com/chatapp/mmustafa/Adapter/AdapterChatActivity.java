package com.chatapp.mmustafa.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import com.chatapp.mmustafa.ChatRoomActivity;
import com.chatapp.mmustafa.Model.Message;
import com.chatapp.mmustafa.R;

public class AdapterChatActivity extends RecyclerView.Adapter<AdapterChatActivity.MyViewHolder> {

    private Context acontext;
    private ArrayList<Message> arrayList;

    public AdapterChatActivity(Context context, ArrayList<Message> arrayList) {
        this.arrayList = arrayList;
        acontext = context;
    }

    @Override
    public AdapterChatActivity.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_chat_room, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Message current = arrayList.get(position);
        holder.tv_name_person.setText(current.getName());
        holder.tv_message_person.setText(current.getMessage());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public CardView mCardView;
        public TextView tv_name_person, tv_message_person;

        public MyViewHolder(View v) {
            super(v);
            mCardView = (CardView) v.findViewById(R.id.card_view_chat_room);
            tv_name_person = (TextView) v.findViewById(R.id.tv_name);
            tv_message_person = (TextView) v.findViewById(R.id.tv_message);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(acontext, ChatRoomActivity.class);
                    acontext.startActivity(intent);

                }
            });
        }

    }

}
