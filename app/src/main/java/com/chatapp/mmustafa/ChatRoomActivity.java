package com.chatapp.mmustafa;

import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.chatapp.mmustafa.Model.ChatMessage;


public class ChatRoomActivity extends AppCompatActivity {
    private static final String TAG = "ChatActivity";

    private ChatRoomAdapter chatRoomAdapter;
    private ListView listView;
    private EditText chatText;
    private ImageButton buttonSend;
    private boolean side = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.chat_room_activity);

        buttonSend = (ImageButton) findViewById(R.id.btn_send);

        listView = (ListView) findViewById(R.id.list_view);

        chatRoomAdapter = new ChatRoomAdapter(getApplicationContext(), R.layout.chat_incoming_msg);
        listView.setAdapter(chatRoomAdapter);

        chatText = (EditText) findViewById(R.id.message);
        chatText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    return sendChatMessage();
                }
                return false;
            }
        });
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                sendChatMessage();
            }
        });

        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listView.setAdapter(chatRoomAdapter);

        //to scroll the list view to bottom on data change
        chatRoomAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                listView.setSelection(chatRoomAdapter.getCount() - 1);
            }
        });
    }

    private boolean sendChatMessage() {
        chatRoomAdapter.add(new ChatMessage(side, chatText.getText().toString()));
        chatText.setText("");
        side = !side;
        return true;
    }
}