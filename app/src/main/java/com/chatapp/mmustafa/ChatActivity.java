package com.chatapp.mmustafa;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import com.chatapp.mmustafa.Model.Message;
import com.chatapp.mmustafa.Adapter.AdapterChatActivity;

public class ChatActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    ArrayList<Message> data = new ArrayList<>();
    SwipeRefreshLayout mSwipeRefreshLayout;
    RecyclerView rv;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity);

        rv = (RecyclerView) findViewById(R.id.rv_chat_message);
        rv.setHasFixedSize(true);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.contentView);
        mSwipeRefreshLayout.setOnRefreshListener(this);


        Message message = new Message("Muhammad Mustafa","First Message");
        data.add(message);
        AdapterChatActivity adapter = new AdapterChatActivity(ChatActivity.this,data);
        rv.setAdapter(adapter);


        LinearLayoutManager llm = new LinearLayoutManager(ChatActivity.this);
        rv.setLayoutManager(llm);

        loadData();
    }

    public void loadData() {
    /*    AndroidNetworking.get(ConfigURL.URL_GET_WORKER_ORDERS)
                .addQueryParameter("workerMobileNo", ConfigURL.getMobileNumber(getActivity()))
                .addQueryParameter("jobStatus", "CANCELED")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        refreshItems();
                        data = new ArrayList<>();
                        Log.d("AA Response", "" + response);

                        try {
                            JSONArray jsonArray = response.getJSONArray("workerJobs");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                String personName, jobTitle, jobId, date;

                                WorkerJobs customer_orders = new WorkerJobs(jsonArray.getJSONObject(i));

                                personName = customer_orders.getCutomerName();
                                jobTitle = customer_orders.getJobTitle();
                                jobId = customer_orders.getJobId();
                                date = customer_orders.getJobCreationTime();

                                Log.d("AA", "" + jobTitle + "" + personName + "");

                                WorkerJobs obj = new WorkerJobs(personName, jobTitle, jobId, date);
                                data.add(obj);

                            }

                            AdapterChatActivity adapter = new AdapterChatActivity(getActivity(), data);
                            rv.setAdapter(adapter);


                        } catch (JSONException e) {
                            refreshItems();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        refreshItems();
                    }
                });


        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);*/
    }

    @Override
    public void onRefresh() {
        loadData();
    }

    public void refreshItems() {
        onItemsLoadComplete();
    }

    public void onItemsLoadComplete() {
        mSwipeRefreshLayout.setRefreshing(false);
    }
}